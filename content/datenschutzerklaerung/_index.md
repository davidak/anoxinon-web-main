+++
draft= false
date = "2017-03-26T08:55:14+02:00"
title = "Datenschutzerklärung"
description = "Anoxinon.de"

+++

<hr>
**I. Allgemeines zur Datenverarbeitung**<br><br>
Wir verarbeiten personenbezogene Daten unserer Nutzer grundsätzlich nur, soweit dies
zur Bereitstellung einer funktionsfähigen Website sowie unserer Inhalte und Leistungen
erforderlich ist. Wir benutzen keine Analyse Software und binden auch keine Drittanbieter (Google Fonts etc.) ein. Zur leichteren Lesbarkeit sind wichtige Informationen im Text fett hervorgehoben.<br><br>
<hr>
**II. Verantwortliche Stelle**<br><br>
Der Verantwortliche im Sinne der Datenschutz-Grundverordnung ist:<br><br>
**Anoxinon e.V.**<br>
c/o Christopher Bodtke<br>
Jahnstraße 3<br>
14513 Teltow<br>
Telefon: (+49) 157 / 3223 9490<br>
Mail: postfach@anoxinon.de<br>
Amtsgericht Potsdam; Registernummer: 9009<br><br>
<hr>
**III. Einsatzgebiet dieser Datenschutzerklärung**<br><br>
Diese Datenschutzerklärung ist, sofern nicht zusätzlich oder anders geregelt, <br>für alle
Domains (TLD) des Anoxinon e.V. sowie den zugehörigen Sub-Domains gültig.<br>
Folgende TLD werden vom Verein betrieben: anoxinon.de, anoxinon.me, anoxinon.media<br><br>
<hr>
**IV. Erhebung und Verarbeitung personenbezogener Daten**

**1. Bereitstellung der Website**

Bei jedem Aufruf unserer Internetseite erfasst unser System automatisiert Daten und
Informationen vom Computersystem des aufrufenden Rechners.
Folgende Daten werden hierbei erhoben:<br>
    • Informationen über den Browsertyp und die verwendete Version<br>
    • Das Betriebssystem des Nutzers<br>
    • Die IP-Adresse des Nutzers<br>
    • Datum und Uhrzeit des Zugriffs  <br>

Eine Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten des Nutzers findet nicht statt.
Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um eine Auslieferung der Website an den Rechner des Nutzers zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert bleiben.<br><br>
Die **Speicherung in Logfiles erfolgt für 72 Stunden in anonymisierter Form**, um die Funktionsfähigkeit der Website sowie die Sicherheit unserer informationstechnischen Systeme zu gewährleisten. **Die IP Adressen werden gelöscht oder verfremdet**, sodass eine Zuordnung des aufrufenden Clients nicht mehr möglich ist.<br><br>
Die Erfassung der Daten zur Bereitstellung der Website und die Speicherung der Daten in Logfiles ist für den Betrieb der Internetseite zwingend erforderlich. Es besteht folglich seitens des Nutzers keine Widerspruchsmöglichkeit.<br><br>
Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Art. 6 Abs. 1 lit. f DSGVO.<br><br>
<hr>

**2. Kontaktaufnahme**

Eine Kontaktaufnahme über die bereitgestellte E-Mail-Adressen ist möglich. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert. Es erfolgt in diesem Zusammenhang keine Weitergabe der Daten an Dritte. Die Daten werden ausschließlich für die Verarbeitung der Konversation verwendet.<br><br>
Die Verarbeitung der personenbezogenen Daten  dient uns allein zur Bearbeitung der Kontaktaufnahme. Im Falle einer Kontaktaufnahme per E-Mail liegt hieran das erforderliche berechtigte Interesse an der Verarbeitung der Daten. Die sonstigen während des Absendevorgangs verarbeiteten personenbezogenen Daten dienen dazu, die Sicherheit unserer informationstechnischen Systeme sicherzustellen.<br><br>
Die Daten bleiben **maximal 3 Monate ab Beendigung der Korrespondenz** gespeichert, es sei denn es bestehen gesetzliche Verpflichtungen, ein berechtigtes Interesse unsererseits oder sonstige vertragliche Verpflichtungen.<br>

Der Nutzer hat jederzeit die Möglichkeit, seine Einwilligung zur Verarbeitung der
personenbezogenen Daten zu widerrufen. Nimmt der Nutzer per E-Mail Kontakt mit uns auf, so kann er der Speicherung seiner personenbezogenen Daten jederzeit widersprechen. In einem solchen Fall kann die Konversation nicht fortgeführt werden.<br><br>
Sollte der Nutzer der Speicherung der Daten widersprechen wollen, kann der **Widerspruch via E-Mail** eingereicht werden. Alle personenbezogenen Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall gelöscht.<br><br>
Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Übersendung einer E-Mail übermittelt werden, ist Art. 6 Abs. 1 lit. f DSGVO. Zielt der E-Mail-Kontakt auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 lit. b DSGVO.<br><br>
<hr>
**V. Auftragsverarbeitung gemäß Art. 28 DSGVO / Weitergabe von Daten**<br><br>
Der Nutzer erklärt sich damit einverstanden, dass durch Beauftragung der folgenden
Unternehmen zur Erbringung von Dienstleistungen Daten an diese weitergegeben werden:<br><br>
    • Hetzner.de: Serverhosting. Sämtliche Daten, die durch Anbieten von Servern und der anoxinon.de Website anfallen, werden auf Serversystemen der Hetzner Online GMBH gespeichert. Verantwortlicher des Unternehmens ist Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen, Deutschland.<br><br>
Auftragsverarbeitungsverträge mit den genannten Unternehmen liegen vor.<br><br>
Es werden keine Daten an Dritte weitergeben, außer sie haben dazu Ihre Einwilligung nach Art. 6 Abs. 1 a DSGVO erteilt, eine gesetzliche Verpflichtung nach Art. 6 Abs. 1 c DSGVO besteht, oder wenn wir dies für die Vertragsabwicklung ( Art. 6 Abs. 1 b DSGVO) sowie die Geltendmachung, Ausübung und Verteidigung von Rechtsansprüchen benötigen.  (Art. 6 Abs. 1 f DSGVO)<br><br>
<hr>

**VI. Datenlöschung und Speicherdauer**

Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch den Gesetzgeber vorgesehen wurde. Eine Sperrung oder Löschung der Daten erfolgt im Anschluss, wenn die vorgeschriebene Speicherfrist abläuft. Es ist möglich dass trotzdem eine Erforderlichkeit zur weiteren Speicherung der Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.<br><br>
Der Betreiber des Dienstes behält sich vor, sämtliche vom Server gespeicherten Daten in Form von Backups für bis zu einem halben Jahr auf externen Systemen vorzuhalten. (Hinweis: Derzeit nutzen wir diesen Rahmen nicht aus und werden nach Evaluierung den exakten Zeitraum an dieser Stelle bekanntgeben)<br><br>
<hr>
**VII. Rechte der betroffenen Person**
<br><br>
***1. Auskunftsrecht***<br><br>
Sie können von dem Verantwortlichen eine Bestätigung darüber verlangen, ob personenbezogene Daten, die Sie betreffen, von uns verarbeitet werden.
Dazu zählen unter anderem: Kategorie der personenbezogenen Daten, Empfänger, Verarbeitungszwecke, Speicherdauer, Herkunft (sofern die Daten aus Drittquellen kommen), Informationen über automatisierte Entscheidungsfindung, deren Details und ggf. Profiling. (Wir führen die beiden letzten Punkte nicht durch)
Ausserdem können sie Auskunft über Ihre Rechte (Beschwerderecht, Löschung, Berichtigung, Einschränkung der Verarbeitung )verlangen.<br>

***2. Recht auf Berichtigung***<br><br>
Sie haben ein Recht auf Berichtigung und/oder Vervollständigung gegenüber dem
Verantwortlichen, sofern die verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig oder unvollständig sind. Der Verantwortliche hat die Berichtigung unverzüglich vorzunehmen.<br>

***3. Recht auf Einschränkung der Verarbeitung und Löschung***<br><br>
Sie können die Einschränkung der Verarbeitung der Sie
betreffenden personenbezogenen Daten verlangen, wenn Sie die Richtigkeit bestreiten, die Verarbeitung unrechtmäßig ist und Sie die Löschung ablehnen und stattdessen die Einschränkung der Nutzung verlangen, wir diese Daten nicht mehr benötigen, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DSGVO eingelegt haben.<br><br>
Sie können weiters von dem Verantwortlichen verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, und der Verantwortliche ist verpflichtet, dies zu tun, außer die Verarbeitung ist erforderlich zur Ausübung des Rechts auf freie Meinungsäußerung und Information; zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses, zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

***4. Recht auf Datenübertragbarkeit***<br><br>
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem
Verantwortlichen bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Außerdem haben Sie das Recht diese Daten einem anderen Verantwortlichen übermitteln zu lassen.

***5. Widerspruchs und Widerrufsrecht***<br><br>
Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.<br><br>
Sie haben ebenso das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen.<br><br>
Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu
betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung zum Zwecke derartiger Werbung einzulegen.

***6. Recht auf Beschwerde bei einer Aufsichtsbehörde***<br><br>
Ihnen steht das Recht auf Beschwerde bei einer Aufsichtsbehörde zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt. Sie können sich grundsätzlich an die zuständige Aufsichtsbehörde Ihres Wohnortes oder Arbeitsplatz wenden.<br><br>
<hr>

**VIII. Schlussworte**<br><br>

Die letzten Änderungen erfolgten am 28.06.2019. <br>Wir behalten uns vor diese Datenschutzerklärung jederzeit zu ändern.<br> Die aktuelle Version ist jederzeit unter diesem Link zu finden.


<br>
<hr>
<br>
<b>
