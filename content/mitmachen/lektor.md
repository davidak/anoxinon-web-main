+++
title = "LektorIn"
description = "Anoxinon Media"
weight = 3
draft = false
bref = "Anoxinon Media"
+++

**Du interessierst dich für Open Source, Datenschutz und natürlich unseren Verein?<br>
Dann bewirb dich bei uns!<br>**

Wir suchen derzeit Verstärkung für unsere redaktionellen Angebote.

Zu deinen Aufgaben würden, unter anderem, die Korrektur und Überprüfung von Inhalten, sowie Überwachung der Einhaltung unserer Richtlinien zählen. Regelmäßige Aktivität ist eine Voraussetzung.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_LektorIn.pdf">PDF Download</a></div>
