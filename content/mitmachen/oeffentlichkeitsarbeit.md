+++
title = "Öffentlichkeitsarbeit"
description = "Anoxinon"
weight = 2
draft = false
bref = "Anoxinon"
+++

**Du interessierst dich für Open Source, Datenschutz, unseren Verein und agierst gerne mit der Community?<br>
Dann bewirb dich bei uns!**<br>

Wir suchen derzeit Verstärkung im Bereich Kommunikations- und Öffentlichkeitsarbeit.

Deine Aufgaben beinhalten unter anderem: <br><br>
Erstellung & Verwaltung von Kampagnen, Verfassen von Inhalten für Anoxinon, Beantworten/Weiterleiten von Nachrichten, Einbeziehen der Community in geplante, neue oder bestehende Inhalte/Angebote. Außerdem gehört die Verarbeitung und Weiterleitung von Anfragen an die richtige Abteilung auch noch mit dazu.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_Oeffentlichkeitsarbeit.pdf">PDF Download</a></div>
