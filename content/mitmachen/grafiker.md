+++
title = "GrafikerIn"
description = "Anoxinon"
weight = 2
draft = false
bref = "Anoxinon"
+++
**Du interessierst dich für Open Source, Datenschutz und natürlich unseren Verein?<br>
Du hast ein Händchen für das Erstellen von Grafiken und/oder Animationen?<br>
Dann bewirb dich bei uns!**<br>

Zu deinen Aufgaben würden, unter anderem, die Erstellung von Thumbnails, Erklärgrafiken (Mindmaps etc.), Flyer und sonstige Überarbeitungen gehören.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_GrafikerIn.pdf">PDF Download</a></div>
