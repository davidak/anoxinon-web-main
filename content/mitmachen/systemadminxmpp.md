+++
title = "Systemadmin - XMPP"
description = "Anoxinon"
weight = 6
draft = false
bref = "Anoxinon"
+++

**Du hast Erfahrung im Umgang mit der Linux Kommandozeile und hast sichere Kenntnisse in Prosody und MySQL? <br>
Du interessierst dich für die Themen Open Source und Datenschutz? <br>
Dann freuen wir uns auf deine Bewerbung!<br>**

Wir suchen derzeit Verstärkung im Bereich Systemadministration.

Deine Aufgaben würden, unter anderem, die Wartung und Absicherung unseres XMPP Servers und dem dahinter liegenden Server beinhalten.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.<br>

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_SysadminXMPP.pdf">PDF Download</a></div>
