+++
title = "Sonstige Unterstützung"
description = "Anoxinon"
weight = 8
draft = false
bref = "Anoxinon"
+++

**Du interessierst dich für Open Source, Datenschutz und natürlich unseren Verein? <br>
Wir freuen uns immer über neue Mitstreiter im  Verein, welche Interesse an unseren Themenbereichen haben!<br>**

Egal wie du dich gerne einbringen würdest, ob als Allrounder oder auf komplett individueller Basis, wir freuen uns immer über Unterstützung!

Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_SonstigeUnterstuetzung.pdf">PDF Download</a></div>
