+++
title = "ModeratorIn - Mastodon"
description = "Anoxinon"
weight = 7
draft = false
bref = "Anoxinon"
+++

**Du interessierst dich für Open Source, Datenschutz, unseren Verein und agierst gerne mit der Community? <br>
Dann bewirb dich bei uns!<br>**

Wir suchen derzeit Verstärkung für die Moderation unserer Mastodon Instanz.

Deine Aufgaben beinhalten unter anderem die Überprüfung von gemeldeten Inhalten, sowie Erste Hilfe bei Problemen und Anliegen rund um die Mastodon Instanz & das Fediverse.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_ModeratorIn_Mastodon.pdf">PDF Download</a></div>
