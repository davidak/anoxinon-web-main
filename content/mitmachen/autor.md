+++
title = "AutorIn"
description = "Anoxinon Media"
weight = 4
draft = false
bref = "Anoxinon Media"
+++

**Du interessierst dich für Open Source, Datenschutz und natürlich unseren Verein?<br>
Dann bewirb dich bei uns!<br>**

Wir suchen derzeit Verstärkung für unsere redaktionellen Angebote.<br>

Zu deinen Aufgaben würden die Mitarbeit an Beiträgen, in Form von
Text/Audio/Video, und entsprechende Recherche gehören.  Wir haben keine vorgeschriebene Anzahl an Beiträgen pro Monat, regelmäßige Aktivität ist aber Voraussetzung.

Es können gerne individuelle Absprachen, bezüglich des Zeitaufwandes, getroffen werden.  Die Bereitschaft zur Unterzeichnung einer Vereinbarung über das Ehrenamt sollte  vorhanden sein. Das wichtigste aber, dass man mitbringen sollte, ist Motivation und Interesse an den Themen des Vereins.

Bei Interesse kannst du dir das PDF, mit sämtlichen Infos rund um die Bewerbung, ansehen.

<div id="action-buttons">
  <a class="button primary big" href="/sb/Stellenausschreibung_AutorIn.pdf">PDF Download</a></div>
