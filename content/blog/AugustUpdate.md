+++
date = "2018-08-15T13:00:00+01:00"
draft = false
author = "Anoxinon"
title = "August Update"
description = "Rückblick und Vorschau, sowie erste Infos zur Vereinsgründung "
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsgründung","rss","anoxinon","fediverse","Mumble", "Peertube", "Website"]


+++

Hallo zusammen!

Bei uns hat sich in letzter Zeit so einges getan, deswegen hier ein kurzer Überblick. :)
<hr>
<h4>Vereinsgründung</h4>
Wir haben beschlossen, den Weg zur Gründung eines gemeinnützigen Vereins zu beschreiten; Eine entsprechende Satzung wird demnächst zur Vorabprüfung beim Finanzamt eingereicht.

Ausschlaggebend für die Vereinsgründung sind u.a .die Aspekte Stabilität und Rechtssicherheit. Sofern eine Person keine Lust mehr hat oder die Umstände einen Fortbestand des Engagements nicht zulassen, gehen unsere Dienste/Server nicht automatisch offline. Ebenso ist der finanzielle Fortbestand besser gesichert. Wir möchten damit dem potenziellen “Instanzsterben” einen Riegel vorschieben, auch wenn jegliche Beteiligung von Privatpersonen an öffentlichen Servern oder Instanzen innerhalb der dezentralen Netzwerke von Bedeutung und auch zu würdigen ist.

Ebenso geplant ist die Finanzierung von Open Source Softwareentwicklung und anderen Projekten, die sich mit unserem Vereinszweck überschneiden; Vorausgesetzt, dass ausreichend Überschuss verbleibt.

Zudem haben wir vor, regelmäßig Beiträge über unsere Themen (Schrift, Audio/VOD) zu veröffentlichen sowie weitere Dienste zur Verfügung zu stellen. Dazu aber in einem der nachfolgenden Blogbeiträgen mehr. ;)

Sollte jemand Interesse haben, uns auf diesem Weg zu begleiten, könnt Ihr uns gerne <a href="https://anoxinon.de/kontakt/">hier</a> kontaktieren. Wir freuen uns auch über Anregungen oder andersweitiges Feedback.
<hr>
<h4>Webseite</h4>
Wenn Ihr durch die Webseite scrollt, bemerkt Ihr vielleicht, dass einiges überarbeitet wurde. Unter anderem wurde die Startseite aktualisiert und an unsere neue Ausrichtung angepasst. Die “Über Uns” Seite beschreibt nun ausführlicher, was wir anpeilen und die “Dienste” Seite wurde umstrukturiert. Betroffen davon sind die Textbausteine und die kommenden Dienste. Bei einem Angebot (TS) ist noch nicht sichergestellt, ob wir das in gewohnter Weise fortsetzen, Hier bleiben erstmal die Entwicklungen abzuwarten. :)

Der Blog wurde ebenfalls ausgemistet. Hier waren teilweise veraltete und nicht mehr zutreffende Beiträge gelistet.
<hr>
<h4>Anoxinon Messenger</h4>
Was fehlt neben einer Microblogging Plattform noch? Korrekt: Eine Möglichkeit zu chatten. Dem wollen wir mithilfe von XMPP Abhilfe verschaffen. Derzeit wird der Server noch eingerichtet.
Es werden aber alle notwendigen XEP's unterstützt und der Server entsprechend datensparsam/abgesichert sein. Soon™ ;)

<hr>
<h4>Monitoring</h4>
Wir haben uns von Uptime Robot verabschiedet und setzen nun auf selbst-gehostete Lösungen. Als Frontend haben wir uns für "Staytus" entschieden (<a href="https://status.anoxinon.de">Klick</a>), da es weniger Abhängigkeiten als "Cachet" mit sich bringt und die Funktionsweisen weitestgehend gleich sind. Im Hintergrund arbeitet eine Open Source Lösung, die uns bei Bedarf über Störungen informiert.

<hr>
<h4>Schlussworte</h4>
Zusammengefasst haben wir viele Pläne. Manche wurden schon in die Tat umgesetzt und weitere sind noch auf dem Weg. Natürlich möchte man am besten alles gleich und schnell, aber wie sagt man so schön? Gut Ding will Weile haben!

In diesem Sinne, vielen Dank für die Aufmerksamkeit und bleibt gespannt! :-)


Grüße,

euer Anoxinon Team
