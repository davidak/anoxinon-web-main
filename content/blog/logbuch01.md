+++
date = "2019-02-28T18:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #01"
description = "Neuigkeiten und Änderungen rund um den Verein"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten","rss", "Website", "Verein"]


+++
Hallo zusammen!

Vergangenen Sonntag hat unsere zweite Mitgliederversammlung stattgefunden und einige Änderungen mit sich gebracht. Aus diesem Grund möchten wir euch über die Ergebnisse informieren. Im Kurzen geht es um einzelne Anpassungen in unserer Satzung, die Freigabe von Inhalten unter einer CC-Lizenz, die Förderung von Projekten und unsere Pläne für das kommende Jahr.

----
**Satzungsänderungen**

Nachdem einige Änderungsvorschläge eingebracht und Klarstellungen innerhalb der Satzung notwendig wurden, haben wir uns mit den einzelnen Punkten auseinander gesetzt und diese entsprechend abgeändert.

• Die Vertretungsberechtigung wurde angepasst und für alle Fälle umgerüstet. Zwei
      Vorstandsmitglieder zusammen vertreten den Verein nach außen.

• Der Schatzmeister ist jetzt stimmberechtigtes Vorstandsmitglied.

• Es wurde über eine Amtszeitbegrenzung für den Vorstand diskutiert, der Vorschlag konnte sich aber nicht durchsetzen.

• In einzelnen Satzungspunkten mussten klarere Formulierungen gewählt werden, für das bessere Verständnis der Satzung selbst.
  Außerdem wurde die Formatierung verbessert.   Vielen Dank an dieser Stelle für die Hinweise.

Neben der Satzung haben wir auch unsere Beitragsordnung angepasst und Ausnahmen hinzugefügt. Die aktualisierten Fassungen stellen wir natürlich umgehend nach Inkrafttreten zur Verfügung.

---
**Lizenzen für unsere Inhalte**

Nachdem wir ein gemeinnütziger Verein sind und selbstlos arbeiten, lag es nahe unsere Inhalte unter eine freie Lizenz zu stellen. Dies dient der besseren Verbreitung unserer Inhalte und schafft Sicherheit für Nutzer eben dieser.
Wir haben uns auf die Lizenz <br>„[Creative Commons 4.0 DE BY](https://creativecommons.org/licenses/by/4.0/deed.de)“ geeinigt. Das bedeutet, dass andere Personen unsere Inhalte nutzen, verbreiten und etwas neues daraus schaffen dürfen. Die einzige Voraussetzung ist eine Namensnennung und ein Hinweis auf die Lizenz. Unter den Vorschlägen war auch das Verbot der kommerziellen Nutzung. Da diese Klausel aber Problemstellungen bei der Verbreitung hervorrufen könnte, haben wir uns dagegen entschieden.

Das Logo und zugehörige Grafiken werdem jedoch nicht unter diese Lizenz fallen. Es wird für diesen Fall ein Pressekit veröffentlicht, welches unter eigenen Bedingungen steht. Wir werden demnächst eine Unterseite mit dem genauen Rahmen und sonstigen Informationen zu der Lizenzierung online stellen.

---
**Förderung Projekte**

Wir haben beschlossen einen Teil unserer jährlichen Überschüsse für die Förderung von Open Source / Free Software Projekten zu verwenden. Die Förderung von Projekten soll anhand des folgenden Schemas stattfinden:


• **Schritt 1 - Einreichungsphase;** Startet am 1. Oktober und dauert 4 Wochen an, innerhalb
dieses Zeitraumes können alle natürliche Personen eine Stimme für ihr bevorzugtes Open
Source Projekt bei uns einreichen

• **Schritt 2 - Auswertungsphase;** In dieser Phase, die zwei Wochen beträgt (1.
November bis 14. November), werden alle eingereichten Stimmen ausgezählt. Die drei Projekte mit den meisten Stimmen, werden der Mitgliederversammlung zur
Abstimmung vorgelegt. Diese entscheidet darüber welches der drei Projekte die Mittel
erhalten soll.

• **Schritt 3 - Spendenaufruf;** Vom 15. November bis 1. Dezember werden wir einen
Spendenaufruf, für das gewählte Projekt, starten. Innerhalb dieses Zeitraumes gehen alle eingegangenen Spenden mit dem entsprechenden Verwendungszweck, zuzüglich zur beschlossenen Fix-summe des Vereins, in den Spendentopf für dieses Projekt.

• **Schritt 4 - Auszahlung;** Pünktlich vor Weihnachten erfolgt dann die Auszahlung des Spendentopf

Diese Form der Unterstützung soll jährlich wiederkehrend, zum Ende des Jahres, stattfinden. In diesem Jahr wird das ganze zum ersten Mal stattfinden. Verfolgt also unsere weiteren Blog Posts gespannt. ;)

---
**Pläne für das kommende Jahr**

Nachdem es einiges Feedback zu unserer Website gab, haben wir uns entschlossen Anpassungen durchzuführen. Auf dem Plan steht eine Neugestaltung der Vereins Website, die bessere Formulierung und Platzierung von Inhalten, sowie die einfachere Navigation und Verfügbarkeit unserer Angebote. Diese Maßnahme soll die Attraktivität und Zugänglichkeit stärken. Ebenso soll es wieder regelmäßige Aktivitäten, außerhalb von Anoxinon.media, geben. Dazu gehört ein monatlicher Beitrag über den Verein und seine Tätigkeiten, also eine Art Logbuch in Form eines Blogbeitrags.

Die Mitgliederversammlung hat sich außerdem dazu ausgesprochen, dem Aufruf der FSFE zu folgen und den Open Letter (Public Money, Public Code) zu unterstützten bzw. zu unterzeichnen.

Zum Schluss haben wir uns noch auf einen Zeitplan geeinigt. Für die nachfolgende Punkte wurde ein voraussichtlicher Zeitraum bis Ende Juli angesetzt.

• Upgrade unseres Prosody (XMPP)

• Code Hosting, in Kooperation mit dem Codeberg e.V.

• Cryptpad

• Peertube

• haste

Unter den Vorschlägen waren auch Dienste wie Searx, Discourse oder Tor Nodes. Diese wurden jedoch im Moment nicht priorisiert und stehen zu einem späteren Zeitpunkt erneut zur Diskussion.

---
Soweit alles Wichtige zur zweiten Mitgliederversammlung.
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
