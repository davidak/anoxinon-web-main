+++
date = "2019-06-28T12:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Wartungsarbeiten 28.06.2019"
description = "Information über anstehende Arbeiten und Änderungen der Datenschutzerklärung"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Server","rss", "Website", "Umzug"]
+++
Hallo zusammen,

in aller Kürze möchten wir euch auf notwendige Wartungsarbeiten hinweisen.

**Heute, am 28.6.2019, werden folgende Dienste nicht oder nur eingeschränkt erreichbar sein:**

* Anoxinon Social
* Interne & Sonstige Anoxinon Dienste

Weitere Informationen erhaltet Ihr auf unserer [Status Seite](https://status.anoxinon.de).

_Außerdem möchten wir auf Änderungen in unseren Datenschutzerklärungen hinweisen. Wir haben den Auftragsverarbeiter (Server Hoster) gewechselt und das bestehende Kontingent erweitert. Alles weitere dazu, gibt es in einem kommenden Blogbeitrag._

Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
