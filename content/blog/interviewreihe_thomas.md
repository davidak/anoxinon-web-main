+++
date = "2018-12-22T18:00:00+01:00"
draft = false
author = "Anxi"
title = "Anoxinon Inside - Teil 2"
description = "Interview mit Thomas Leister"
categories = ["allgemeines", "verein", "interview"]
tags = ["Verein","Interview","anoxinon", "gründung"]

+++
Nachdem es größeres Interesse  an den Personen hinter dem Verein gab, haben wir beschlossen eine kleine „Interviewreihe“ zu veröffentlichen.
Mein Name ist Anxi, <br>ich bin das Maskottchen von Anoxinon und führe durch das Interview. :)<br>
Mein heutiger Gast ist Thomas Leister, Vorstandsmitglied des Anoxinon e.V.
<hr>

**Anxi: Hallo Thomas, stell Dich doch mal unserer Community vor!**

Thomas: Hi Leute, ich bin der Dude von thomas-leister.de, trashserver.net und metalhead.club. ;-) Ich bin Linux-Fan und ein Freund freier Software, mithilfe derer wir uns unabhängig von den großen Service Providern machen können.


**Anxi: Womit beschäftigst Du Dich in deiner Freizeit? Was macht Dir am meisten Spaß?**

Thomas: In meiner Freizeit höre ich sehr gerne Musik (Metal, Rock, ein bisschen Elektro) und beschäftige mich mit Linux und Linuxservern.
Ansonsten darf auch das Feiern im Freundeskreis nicht fehlen.


**Anxi: Welches Betriebssystem verwendest Du und wieso?**

Thomas: Seit mehr als zwei Jahren verwende ich Fedora Workstation („Linux“) auf meinen Desktop-Rechnern. Angefangen hat es mit Ubuntu (welches nach jedem Upgrade kaputt war), weiter ging es einige Jahre mit Arch (das mir irgendwann zu viel Bastelarbeit beschert hat) und nun bin ich bei Fedora hängen geblieben, dass sich für mich als ziemlich zuverlässig und pflegeleicht erwiesen hat.


**Anxi: Was hat dein Interesse an Linux geweckt? Wie waren deine ersten Erfahrungen in diesem Bereich?**

Thomas: 2009 habe ich meine ersten Gehversuche mit der Website Entwicklung in HTML und PHP gemacht. Mein Onkel hatte vorgeschlagen, einfach einen alten Rechner mit Ubuntu auszustatten und einen Webserver inkl. PHP-Umgebung darauf zu betreiben. Einen eigenen Webspace hatte ich damals noch nicht. Hilfe und Rat habe ich mir damals vor allem aus dem Internet geholt. Es war nicht ganz einfach mit dem komplett neuen Betriebssystem alleine zurecht zu kommen, aber ich habe mich durchgebissen und nach und nach auch andere Dinge entdeckt die man mit Linux umsetzen konnte. Zum Beispiel NAS-Server oder später Mailserver.

Meinen ersten „richtigen“ Server, in einem Rechenzentrum, habe ich Ende 2011 angemietet und darauf einen XMPP-Server betrieben. Mangels besserer Einfälle nannte ich ihn einfach trashserver.net. :-)

Seitdem habe ich viel dazugelernt und bin weiterhin neugierig darauf neues zu entdecken. Vor kurzem habe ich Linux zu meinem Beruf gemacht und verdiene damit mein täglich Brot.

**Anxi: Wie ist deine Einstellung zu dezentralen Netzwerken, sowie Open Source/Free Software? Gibt es etwas dass dich besonders fasziniert ?**

Thomas: Wenn sich etwas gewinnbringend dezentralisieren lässt, sollten wir das tun. Wenn es Sinn macht Software unter einer freien oder offenen Lizenz zu publizieren, sollten wir das ebenfalls tun. Nur wenn wir einen Teil unseres Wissens anderen zur Verfügung stellen, kommen wir als Gesellschaft voran. Das zeigt das Linux Projekt ganz hervorragend: Wenn viele Menschen ihr Fachwissen beisteuern und teilen, kann großartiges entstehen und jeder einzelne hat etwas davon. Womit würden wir zum Beispiel unsere Fritz-Boxen betreiben, wenn es Linux nicht gäbe? Mit Windows? Kann man machen, aber Lizenzkosten und eine Monokultur sprechen beispielsweise dagegen.

Ich bin kein Open Source / Freie Software Fanatiker. Closed Source Software bzw. proprietäre Software hat durchaus Ihre Daseinsberechtigung und kann im freien Wettbewerb dabei helfen Alleinstellungsmerkmale zu entwickeln. Wenn es aber um eine gemeinsame Basis geht, auf der wir arbeiten oder kommunizieren wollen, sollten wir das frei und offen tun und auf ebensolche Plattformen und Software setzen.


**Anxi: Gab es Ereignisse die maßgeblich zu dieser Einstellung beigetragen haben?**

Thomas: Es gab nicht das Ereignis, das mich zu einem Fan freier Software gemacht hat. Es waren viele kleinere Erlebnisse die mir gezeigt haben dass es Sinn macht diese Art von Software zu fördern und zu verwenden:

* Ohne freie Software hätte ich mir in jungen Jahren einen solchen Einstieg in die IT bei weitem nicht leisten können.
* Sicherheitslücken sind in der Vergangenheit häufig nur aufgefallen, weil fremde Personen Einsicht in den Quellcode hatten.
* Ich kann Fehler in Software selbstständig beheben, wenn ich das will. So zum Beispiel geschehen beim VLC Player.
* Freie Software kann auch eine Art sein „Danke“ an andere Entwickler zu sagen, wenn man sich daran beteiligt eigene Software unter freien Lizenzen zur Verfügung zu stellen.
* Durch freie Software kann ich mich kreativ austoben und Erweiterungen entwickeln oder den Quellcode modifizieren.
* Freie Software ist wichtig wenn man Macht verteilen will.


**Anxi: Welche Themen bekommen zu wenig Aufmerksamkeit, deiner Meinung nach? Wo ist noch Nachholbedarf?**

Thomas: Äußerst ärgerlich finde ich, wenn ich mit Steuergeldern staatliche Software finanziere, die dann verschlossen gehalten wird. Wenn wir Bürger öffentliche Gelder verwenden, muss die Software auch uns gehören bzw. wir müssen ein Recht haben, diese nach Belieben zu verwenden. Jeder einzelne von uns.

Ein anderes Thema dass mir sehr am Herzen liegt ist die Vermeidung von IT-Monokulturen, starken Abhängigkeiten und Vendor Lock-ins. Das betrifft sowohl Unternehmen als auch Privatleute. Wir sollten stärker darauf achten Kommunikation und Informationsverarbeitung besser zu verteilen, unabhängiger zu gestalten und Möglichkeiten zu schaffen durch Standardisierung dennoch kompatibel zu bleiben.

Zuletzt bedeutet mir auch das Thema „Datenschutz“ viel. Informationen sollten nur nach meinem Wissen gesammelt und verarbeitet werden. Die neue Datenschutzgrundverordnung ist ein guter Schritt in die richtige Richtung (auch wenn ich mir über die Mehraufwände natürlich im Klaren bin). Es geht mir nicht darum gar keine Informationen über mich Preis zu geben. Es ist legitim Informationen an Unternehmen zu geben. Wenn ich aber Informationen über mich Preis gebe muss ich davon ausgehen können, dass diese nur zu den vereinbarten Zwecken verwendet und nicht missbraucht werden.


**Anxi: Wie siehst du die Entwicklungen in der Zukunft?**

Thomas: Als größtes Sorgenkind sehe ich die Konzentrierung des Internets auf ein paar wenige, mächtige Institutionen. Wenn es so weitergeht ist das Internet bald vielleicht nur noch so etwas wie Pay TV. Ich sehe derzeit (trotz aller Anstrengungen aus „unserer“ Community) nicht, dass sich der Trend umkehrt. Ganz im Gegenteil: Immer mehr Menschen verlassen sich zum Beispiel auf die Google Cloud oder auf Amazon AWS.


**Anxi: Anderes Thema: Warum bist Du dem Verein Anoxinon beigetreten?**

Thomas: Ich hoffe, durch meine Beteiligung und der Hilfe der Vereinsmitglieder ein größeres Bewusstsein für informationstechnische Freiheiten zu schaffen. Es *gibt* Alternativen. Wir *können* es besser machen. Wir müssen es nur wollen und diese Alternativen intensiver bewerben.

Der Verein ermöglicht einen intensiveren Austausch, die Bündelung von Kräften und eine bessere Koordination unserer gemeinsamen Aktionen  damit wir unserem Ziel ein Stück näher kommen.


**Anxi: Worin siehst Du Deine Aufgabe im Verein? Was ist dein persönliches Ziel in und für dem Verein?**

Thomas: Meine Aufgaben sehe ich vor allem bei der Pflege und Wartung unserer Vereins-eigenen Infrastruktur und der Wissensvermittlung.


**Anxi: Danke für das Gespräch :)**


<hr>
