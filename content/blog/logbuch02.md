+++
date = "2019-04-06T10:20:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #02"
description = "Neues vom Dampfer"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten","rss", "Website", "Verein"]

+++
Hallo zusammen,

mit etwas Verzögerung möchten wir euch über die aktuellen Neuigkeiten informieren.<br>
Ohne Großes drum und dran, fangen wir gleich an! ;)

----
**Bank/Satzungsänderungen**

Wir besitzen seit Ende März ein Vereinskonto bei der Triodos Bank. Nach einigem Hin und Her, dank verschiedenster äußeren Einflüsse, konnten wir
das Thema endlich abhaken. Im Anschluss haben wir mit der Bearbeitung aller aufgestauten Angelegenheiten begonnen. Wir können jetzt auch Spenden annehmen. Eine entsprechende Ergänzung auf der Homepage werden wir in Kürze online schalten. :)

Wir hatten außerdem auf der Mitgliederversammlung beschlossen, dass wir die
Satzungsänderungen erst in Kraft treten lassen wenn ein Bankkonto vorhanden ist, das Finanzamt
und das Amtsgericht nichts gegen diese einzuwenden haben. Einen Notartermin haben wir auf
Anfang Mai fixiert und die Unterlagen dem Finanzamt zur Vorabprüfung zukommen lassen. Wir
gehen aber nicht davon aus, dass es hier zu Einwänden kommen wird.

----
**Prosody**

Der erste Punkt unseres Zeitplans bis Ende Juli, wurde in den letzten Tagen erledigt. Dabei ging es
um das Upgrade unseres XMPP Servers "Anoxinon.me". Nebst dem Upgrade gab es auch einige
neue Features, wie eine verbesserte Speicherung von Offline Nachrichten und eine Verbesserung
von OMEMO.

----
**Codeberg e.V.**

Im Anschluss an das Prosody Upgrade haben wir unseren Code, wie besprochen, auf die Plattform
des Codeberg e.V. verschoben und eine Fördermitgliedschaft beantragt. Das Code Repository ist
[hier](https://codeberg.org/Anoxinon_e.V.) einsehbar. Damit unterstützen wir einen Verein der sich
klar zum Ziel bekannt hat Open Source Software, ebenso wie wir, zu fördern.

In Zukunft werden wir weitere Kooperationen anstreben.

---
**Cryptpad / Peertube**

Nachdem Prosody und die Auslagerung der GIT abgeschlossen sind, steht
als nächstes die Bereitstellung des internen Cryptpads für die Öffentlichkeit an. Neben Design-
technischen Änderungen, müssen entsprechende rechtliche Texte verfasst, die Limits für Accounts
festgesetzt und weitere Anpassungen im Backend erledigt werden.

Im Anschluss kümmern wir uns dann um Peertube. Hier sind ähnliche Anpassungen wie bei dem Cryptpad notwendig.
Es muss außerdem ein entsprechendes Konzept für die Verwaltung der Videos und des notwendigen Speicherplatzes entworfen werden.
Voraussichtlich wird in der ersten Phase die Peertube Instanz nur für Inhalte des Vereins, und
ausgewählten anderen Akteuren, zur Verfügung stehen. In der zweiten Phase können dann alle
Nutzer Ihre Inhalte via unserer Peertube Instanz teilen. Wir halten euch diesbezüglich weiter auf dem Laufenden. :)

---
**Website**

Demnächst wird fleißig an der Vereinshomepage Anoxinon.de gebastelt. Dazu zählt die einfachere
Navigation, die bessere Formulierung und Platzierung unserer Inhalte und eine ggf. notwendige
Neugestaltung der Seite. Vor allem die Startseite, die Übersicht über unsere Dienste, die
Stellenbörse und die "Über Uns" Seite stehen im Fokus. Es gab relativ viel Feedback diesbezüglich
und das ganze lässt sich nicht von heute auf morgen erledigen. Wir werden jedoch Schritt für Schritt
entsprechende Änderungen umsetzen.

---
Soweit alles Wichtige aus der letzten Zeit.<br>
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
