+++
date = "2019-07-09T17:50:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #05"
description = "Neues aus dem Vereinsleben, Server Migration, Förderation uvm."
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten","rss", "Website", "Verein"]

+++
Hallo zusammen,

heute gibt es, leicht verspätet, wieder einen Rückblick auf die letzten Wochen.
Es sind ein paar Dinge passiert, also fangen wir gleich an. :)

---

**Finanzen, Änderungen in der Server Struktur**

Anfang Juni hat sich der Vorstand entschlossen, eine Maßnahme zur Verbesserung der Wirtschaftlichkeit des Vereins umzusetzen. Nach einem Kassensturz durch den Schatzmeister wurde festgestellt, dass der Verein finanziell zwar genügend Geld zur Verfügung hat um die laufenden Betriebskosten zu decken, jedoch andere notwendige Investitionen zur Expansion des Vereins nicht möglich sind. Außerdem war ein weiterer Abbau der offenen einmaligen Kosten nicht möglich. Der Schatzmeister hat, vor der Vorstandssitzung, um Evaluierung bzgl. der Senkung von den Betriebskosten gebeten.

Aus diesem Grund haben wir, in den letzten beiden Wochen, unsere Serverinfrastrukur  von Servercow, zur Hetzner Online GMBH migriert. Ursprünglich war unsere alte Infrastruktur für alle kommenden Dienste ausgelegt. Da diese aber länger auf sich warten lassen, haben wir sehr viele freie Ressourcen zur Verfügung gehabt.
Durch radikale Einsparungen konnten wir die Kosten, kurzfristig, um etwa die Hälfte senken. In Kürze werden wir die Transparenzinformationen auf unserer Website aktualisieren. Folgekosten, im Zuge des Umzugs, sind möglich. Anzumerken sei hierbei noch, dass die Kosten mittel- oder langfristig wieder steigen werden. Ein weitere Migration ist nicht ausgeschlossen. Servercow hat den Verein, noch bevor er offiziell gegründet wurde, großzügig unterstützt und uns mit viel Geduld und Enthusiasmus begleitete. Die Qualität war in keiner Hinsicht zu bemängeln. Und eine Rückkehr zu einem späteren Zeitpunkt ist definitiv nicht ausgeschlossen. :)

---

**Personelle Entwicklungen**

Derzeit sind für den Verein sechzehn Personen als Ehrenamtliche Mitarbeiter tätig, vier davon entfallen auf den Vorstand. Wir haben im Monat Juni ein Time Tracking Tool ausgetestet, hierbei geht es rein um die Erfassung des allgemeinen Arbeitsaufwand. Nicht alle Personen haben es verwendet, die Testphase geht noch bis Ende Juli.
Eine Person hat einen Pauschale eingetragen, die auch dem tatsächlichen Aufwand entspricht. Folgendes Ergebnis gab es: 8 Personen haben, zwischen 1. und 30. Juni, ca. 153 Stunden ehrenamtliche Arbeit geleistet.

Seit etwa zwei Wochen, haben wir einen neuen Redaktionsleiter. Der bisherige Leiter hat diese Aufgabe abgegeben, um sich auf die anderen Aufgaben, gemäß seines Vorstandspostens, konzentrieren zu können. Derzeit läuft noch eine Übergangsphase.

Außerdem haben wir unsere <a href="/mitmachen/">"Mitmachen"</a> Seite aktualisiert und die PDFs ausgetauscht. Derzeit suchen wir, ins besonders, Personen für Grafiken, Öffentlichkeitsarbeit und Lektorat. Wenn also jemand Lust hat, oder Personen kennt die aktiv werden möchten, meldet euch! :)

---

**Förderationsrichtlinien – Mastodon & Allgemein**

Da es auch bei uns ein paar Fragen zu unseren Richtlinien, bzgl. der Föderation mit anderen Instanzen, gegeben hat, möchten wir einmal unsere Handhabung klarstellen.

Der User selber besitzt die Möglichkeit andere Personen stummzuschalten oder zu blockieren. Sollte eine Aktion durch den User, die weniger einschneidende und gerechtfertigte Maßnahme sein, werden wir nicht ein schreiten.

Wenn hingegen eine Instanz mit uns föderiert und Inhalte verbreitet, die gegen deutsches o. europäisches Recht verstoßen, werden wir aktiv. In der Regel reagieren wir zuerst mit der Sperrung o. Stummschaltung von einzelnen Konten, dann mit der Stummschaltung von Instanzen und als letzte Möglichkeit die vollständige Blockade. Die Maßnahmen werden individuell getroffen, das Ausmaß und der Umfang unterscheiden sich von Fall zu Fall. Sie werden in regelmäßigen Abständen evaluiert. Klar ist aber: Diese Handhabung gilt für alle Instanzen, wir machen hier keine Ausnahme.

Anzumerken sei noch, dass Instanzen in der Regel auf die Verstöße aufmerksam gemacht werden. Wenn die Betreiber aber nicht aktiv werden wollen, oder dringender Handlungsbedarf besteht, setzen wir eigene Maßnahmen um. Als Beispiel möchten wir hier die Verbreitung von pornographischen Material oä. nennen. Wir werden in den kommenden Tagen unsere Liste mit blockierten oder stummgeschalteten Instanzen aktualisieren und fehlende Begründungen nachtragen.

---

**DBJR**

Am 26.6.19 haben wir, mit zwei Personen, an dem Workshop der Reihe „Digitale Jugendarbeit“, des Deutschen Bundesjugendring (DBJR), teilgenommen. EinVertreter des DBJR hat uns eingeladen, an der Veranstaltung teilzunehmen. Der Workshop war in zwei Bereiche gespaltet, einerseits in einen Theorieteil und andererseits in einen Praxisteil.

Am Vormittag ging es los mit dem Theorieteil und dem Arbeitstitel „Smart Youth Work“. Eine <a href="https://www.dbjr.de/artikel/smart-youth-work/">ausführliche Begriffserklärung</a> kann man auf der Homepage des DBJR nachlesen. Zuerst wurden verschiedene Aspekte und zugehörige Themen auf einer Pinnwand aufgeschrieben.  Im Anschluss wurden die Themenschwerpunkte und Fragen besprochen, sowie diskutiert. Hier ging es um Fragen, wie man in der gesellschaftlichen Debatte mithält, was man noch wie umsetzen muss, den Wandel und den Transformationsprozess hin zur „Smart Youth Work“.
Zugehörige Themenschwerpunkte waren: Jugendarbeit, Jugendpolitik, Qualifizierung und Vernetzung. Digitale Ethik, Kritische Digitalkompetenz, Aus- und Weiterbildung, Grundstandards, Innovation & Stabilität, sind nur ein paar der ausführlicheren Überlegungen gewesen. Da es sich um eine dreiteilige Workshop Reihe handelt, war diesmal das Ziel, sich einen Überblick zu verschaffen und Ansätze zu entwickeln, die man dann in den nachfolgenden Workshops weiter ausarbeitet. Die Zeit wäre auch zu knapp gewesen, um detaillierte Konzepte auszuarbeiten.

Kurz vor der Mittagspause, haben sich die Teilnehmenden in kleine Gruppen aufgeteilt und sollten drei Schlagwörter, die Ihnen besonders wichtig erschienen, reihen. Im Anschluss sollten die einzelnen Gruppen dann kurz darstellen wieso und warum Sie die Entscheidung getroffen haben. Die Gruppe, in der wir waren, hat sich etwas ins Gespräch vertieft und die Zeit dezent überzogen, also haben wir uns nur auf „Innovation & Stabilität“ festgelegt. (Anm. d. Autors: Leider sind meine Notizen nicht ausführlich genug gewesen, es gab dann noch spontane Festlegung des Platz 2 & 3.)

Der Grundtenor war aber, dass man nicht auf jeden neuen „Hypetrain“ aufspringen soll und die Digitalisierung als unterstützende Maßnahme, zur bestehenden Arbeit sehen soll. Gleichzeitig ist es aber zu vermeiden, schwerfällig zu werden und die gesellschaftlichen Entwicklungen zu verschlafen. Positiv anzumerken ist, dass Themen, wie Datenschutz, bei den Vertretern vor Ort nie aus dem Hinterkopf verloren wurde. Jugendorganisationen haben schon immer striktere Datenschutzregeln gehabt, es war aber schön zu sehen, dass man sich seiner Verantwortung bewusst ist. Bei der Einführung der DSGVO gab es dadurch keine wirklichen Probleme.

Nach der Mittagspause und ein paar Gesprächen ging es, im Praxisteil am Nachmittag, um Kollaboratives Arbeiten. Am Anfang stellte einer der Workshop Organisatoren Nextcloud vor und sprach sowohl über die Vor- als auch die Nachteile der Anwendung. An dieser Stelle einen großen Daumen nach oben, für diese reflektierte, aber doch positive Präsentation. Weiter ging es mit anderen Tools und Erfahrungsaustausch, sowie die Evaluierung von individuellen Bedürfnissen und Lösungen.

Alles hier geschriebene ist stark heruntergebrochen, ein ausführlicheres Resümee werden wir am Ende der Workshopreihe veröffentlichen. Wir sind voraussichtlich bei Part 2 & 3, wieder vor Ort.

---

**BMFSJ - Innovationsbüro, Chancen Hackathon**

Von 14.6 bis 15.6.19, war unser 1. Vorsitzender, auf dem Chancen-Hackathon des Bundesministerium für Familie, Senioren, Frauen und Jugend. Bei dieser zweitägigen Veranstaltung ging es, unter anderem, um die Frage wie die Gesellschaft im ins-gesamten von der Digitalisierung profitieren kann.
Ein eigenständiger Blog Beitrag wird diesbezüglich noch nachgereicht.

---

**Mumble Stammtisch**

Zum Schluss noch ein weiterer Hinweis auf unseren Mumble Stammtisch. Dieser findet
regelmäßig Sonntags, ab 20 Uhr, statt. Wir reden dort über verschiedene Themen, auch abseits des
Vereins. Dazukommen kann jeder der Lust hat, ob Vereinsmitglied oder nicht. :)

In Zukunft werden wir auf der Website, einen kleinen "Kalender" mit kommenden Daten zur Verfügung stellen. Es ist geplant, zumindest einmal im Monat, ein festes Thema ausführlich zu behandeln.

---

Soweit alles Wichtige.   
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
