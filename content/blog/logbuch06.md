+++
date = "2020-01-05T21:09:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #06"
description = "Neues aus dem Vereinsleben, Rückblick und Vorschau"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten","rss", "Website", "Verein"]

+++

Hallo zusammen,

heute gibt es Neuigkeiten aus dem Vereinsleben, einen kurzen Rückblick und eine Vorschau auf das kommende Jahr.

---

**Neuigkeiten aus dem Vereinsleben**

In unserem letzten Blogbeitrag, vom 20.12.19, haben wir öffentlich auf die angespannte Situation hingewiesen und nach neuen Vorstandsmitgliedern gesucht. Vielen Dank für das Feedback und an diejenigen die Interesse bekundet haben.

Nach eingehender Diskussion und einer zugehörigen Reflexion wurden folgende vier Personen in den Vorstand gewählt:

* Christopher Bodtke, alias SkyfaR, bleibt 1. Vorsitzender da er durch weitere Personen entlastet wird. Er hat die technischen Agenden abgegeben, hilft aber bei Bedarf noch aus.
* Michael Gulden, alias Angrytux, war bereits Mitglied und ehrenamtlich tätig und ist nun Vorstandsmitglied.
* Malte Kiefer, alias beli3ver, ist neu dazugekommen und wird als Vorstandsmitglied die technische Leitung übernehmen.
* Patrick von den Driesch alias ipatrick1990, bleibt weiterhin Schatzmeister.

Thomas Leister scheidet aus zeitlichen/beruflichen Gründen aus und bleibt uns als ehrenamtlicher Mitarbeiter und Mitglied des Vereins im Bereich Technik erhalten. An dieser Stelle sei Dir noch einmal für Deinen Einsatz und die vielen Mühen gedankt. :)

Durch den verstärkten Vorstand kann die Arbeitslast besser bewältigt werden. Aus diesem Grund machen Christopher und Patrick auch im Vorstand weiter. Wir können also Entwarnung geben: **Der Verein bleibt bestehen und es werden keine Dienste eingestellt.**

---

**Rückblick & Vorschau**

Durch die eingeschränkten Kapazitäten musste vieles in den letzten Monaten liegenbleiben. Im nachfolgenden möchten wir ein paar Punkte ansprechen, die in Zukunft besser gemacht werden sollen.

1. Wir hatten ein Defizit in der Öffentlichkeitsarbeit und dem Auftritt nach außen. Das beinhaltet das fehlende regelmäßige Logbuch, die Aktivität im Fediverse usw. Ebenfalls verschoben wurde unser Förderprogramm für Open Source Software, dass ursprünglich Ende 2019 über die Bühne gehen sollte.
2. Die notwendige Überarbeitung der Inhalte auf unserer Vereinswebseite, sowie das neue Design sind zum Endes des Jahres nur langsam vorangekommen, sodass wir die geplante Erscheinung auf das Q1 2020 verschoben haben. Die Veränderung der Inhalte ist notwendig geworden, da es für einige bisher nicht sofort ersichtlich war welchen Zweck der Verein verfolgen möchte und wir allgemein viele Texte detaillierter sowie präziser formulieren wollen, um ein runderes Gesamtbild abzugeben.
3. Soweit es uns möglich ist, planen wir in Zukunft unsere Offline Aktivitäten zu verstärken und den Output online zu erhöhen. Da diese beiden Faktoren aber stark von der verfügbaren Anzahl an Personen abhängig ist, möchten wir auf unsere "[Mitmachen](https://anoxinon.de/mitmachen/)" Seite verweisen. Im Moment suchen wir vor allem in den Bereichen [Redaktion](https://anoxinon.de/mitmachen/autor/), [Öffentlichkeitsarbeit](https://anoxinon.de/mitmachen/oeffentlichkeitsarbeit/) und [Lektorat](https://anoxinon.de/mitmachen/lektor/) nach weiteren Mitstreitern.

Abgesehen davon, gibt es noch einige kleinere Dinge die liegen geblieben sind. Diese sollen aber genauso angegangen werden. :) Gerne würden wir auch Euer Feedback diesbezüglich hören.

---

**Personelles/Finanzen**

Für den Verein sind derzeit 11 ehrenamtliche Mitarbeiter in diversen Bereichen tätig, exklusiv des Vorstandes. Die Mitgliederzahl beläuft sich auf 16, im Dezember kam ein Mitglied dazu.

Auf der ersten außerordentlichen Mitgliederversammlung, vom 10.11.2019, wurde eine Änderung der Beitragsordnung beschlossen. Ziel ist es die Mitgliedsbeiträge mittelfristig zu senken, um attraktiver für neue Leute zu werden, aber auch gleichzeitig liquide zu bleiben. Die Beiträge werden hierbei monatlich angepasst und stufenweise gesenkt bis zu einem Maximum von:

1. 10€ (NOR)
2. 7,50€ (ERM1)
3. 5€ (ERM2)

Diese Regelung tritt in Kraft ab einer Mitgliedsanzahl von 17 Personen oder mehr. Pro dazugekommenen Mitglied wird der Beitrag um 1,00 € gesenkt, mit dem 21. ist dann die Untergrenze erreicht. Sollte die Mitgliederzahl unter 21. fallen, wird der Beitrag wieder stufenweise angehoben. Der aktuelle Beitrag wird in Zukunft auf dem [Mitgliedsantrag](https://anoxinon.de/files/Mitgliedsantrag.pdf) und auf der Website monatlich aktualisiert.

Außerdem wurden die [Transparenzinformationen](https://anoxinon.de/unterstuetzung/) auf den Stand 1.1.2020 aktualisiert.

---

**Sonstiges**

Des Weiteren wurden wir auf den Neujahresempfang der SPD Ortsvereine und des Landtagsabgeordneten Herrn [Sebastian Rüter](http://www.sebastianrueter.de/) für den Raum Teltow, Kleinmachnow, Stahnsdorf und Nuthetal eingeladen. Als Ehrengast wird Frau [Manja Schüle](https://mwfk.brandenburg.de/mwfk/de/ministerium/ministerin-manja-schuele/), Ministerin für Wissenschaft, Forschung und Kultur des Landes Brandenburg erwartet. Durch unsere Anwesenheit bei diesem Empfang möchten wir nicht nur uns verstärkt lokal vernetzen mit anderen Vereinen, um Synergien zu schaffen, sondern auch mehr Aufmerksamkeit auf unsere Vereinsthemen lenken. Wenn es diesbezüglich Neuigkeiten gibt, berichten wir darüber natürlich im nächsten Logbuch. :)

Wie gewohnt werden die beiden neuen Vorstandsmitglieder nochmal in einem separaten Interview mit Anxi vorgestellt.

Was Euch noch alles im Jahr 2020 erwartet werdet Ihr im nächsten Logbuch im Februar erfahren, sobald unsere Mitgliederversammlung stattgefunden hat.

---

Soweit alles Wichtige.\
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße

euer Anoxinon Team
