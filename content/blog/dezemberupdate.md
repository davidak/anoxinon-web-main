+++
date = "2018-12-17T18:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Dezember Update"
description = "Aufnahme des Geschäftsbetriebs, Informationen zum Umzug"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsgründung","rss","anoxinon","fediverse","XMPP", "Messenger", "Website"]


+++

Liebe Nutzer,

zum nahenden Ende des Jahres 2018 haben wir ein paar Neuigkeiten für euch! Im Kurzen geht es um die Aufnahme des Geschäftsbetriebs (Mitgliedschaft, Stellenbörse etc.), einem neuen Dienst und die Fristsetzungen von Änderungen der Datenschutzerklärungen für Mastodon und XMPP.

<hr>


**Aufnahme des Geschäftsbetriebs**
<br><br>
Am vergangenen Freitag, dem 14.12.2018, erreichte uns der Bescheid des Amtsgerichts Potsdam über die erfolgreiche Eintragung ins Vereinsregister. Damit ist die größte Hürde einmal geschafft und wir dürfen, laut Beschluss der Gründungsversammlung, nun den Betrieb offiziell aufnehmen. Wir haben bereits beim Finanzamt um die Anerkennung unserer Gemeinnützigkeit angesucht, da aber ein paar Feiertage vor uns liegen rechnen wir nicht vor der ersten Januar Woche mit einer Antwort.<br><br>

**Ab heute nehmen wir auch Mitgliedschaftsanträge an. Sollte jemand Interesse haben uns zu unterstützen oder selber mitzubestimmen, kann er das nun tun.**  <br>
Wir haben sämtliche Dokumente zur Mitgliedschaft, Fördermitgliedschaft, Satzung & Co unter dem Menüpunkt „Über uns“ zur Verfügung gestellt.<br>
Ebenso findet Ihr dort eine kleine Stellenbörse, diese wird in den nächsten Tagen noch mit detaillierten Beschreibungen aufgefüllt. Derzeit sind wir hauptsächlich auf der Suche nach Autoren und Lektoren, die unser kleines Team bereichern können. :)<br>
Wir denken das es selbstverständlich ist dass unser Vorhaben nicht von nur einer Handvoll Leute bewältigt werden kann. Aus diesem Grund sind wir immer auf der Suche nach interessierten Mitstreitern die uns tatkräftig Unterstützen wollen!<br><br>

<hr>
**NEU: Anoxinon Media**

Neben dem Angebot von dezentralen Diensten, wie XMPP/Mastodon, möchten wir auch gleichzeitig Aufklärung über unseren Themen betreiben. Wir haben festgestellt, dass sich trotz vieler Informationen und Aufklärungsarbeit, noch immer ein großer Teil von Menschen keine Gedanken um die potenziellen Gefahren und Möglichkeiten der Technik, sowie grundsätzliche Fragen wie die des Datenschutzes und der informationellen Selbstbestimmung gemacht haben. Leider sind die „Tipps“ von manch einem großem Medium, mehr Pest und Cholera als wirkliche Lösungen. Derzeit verlassen sich die meisten im Thema Datenschutz, auf dubiose zentrale Anbieter aus dem Ausland die Ihr Hauptgeschäft mit dem Verkauf von Daten und personalisierter Werbung machen.  <br><br>
Aus diesem Grund haben wir beschlossen eine Plattform für Inhalte in Form von Text/Audio und später Videos zu gründen. Diese sollen in unterschiedlichen Schwierigkeitsstufen erscheinen. (Anfänger, Fortgeschritten, Profi) Insbesondere möchten wir die Themen Datensicherheit, Verschlüsselung, Datenschutz, Kryptographie und Zensur behandeln. Darunter fallen natürlich auch Begleitthemen wie Open Source.<br><br>
Die Plattform wird voraussichtlich Ende Dezember online gehen. Der Output ist abhängig von personellen und zeitlichen Ressourcen, es sind aber aktuell 2 Beiträge pro Monat geplant.
Wir sind nicht die einzigen die auf solchen Gebieten informieren, denken aber dass eine gute Ergänzung möglich und weitere Aufklärungsarbeit/Information in diesen Bereichen essenziell wichtig ist.<br><br>

<hr>

**Änderungen Datenschutzerklärung XMPP/Mastodon, Betreiberwechsel**

Nachdem wir unseren Betrieb aufgenommen haben, wird es an der Zeit die Dienste vollständig auf den Verein zu überschreiben. Der Stichtag dafür ist der 07.01.2019.
<br><br>
Betreiber alt:
<br>
Christopher Bodtke, Jahnstraße 3<br>
14513 Teltow<br>
Telefon: (+49) 157 / 3223 9490, Mail: postfach@anoxinon.de<br>
<br>
Betreiber neu:<br>
**Anoxinon e.V.**<br>
c/o Christopher Bodtke<br>
Jahnstraße 3, 14513 Teltow<br>
Telefon: (+49) 157 / 3223 9490, Mail: postfach@anoxinon.de<br>
Amtsgericht Potsdam; Registernummer: 9009<br><br>
Wir haben dies zum Anlass genommen die Datenschutzerklärungen anzupassen, sowohl vom Inhalt als auch von der Struktur her, und haben ein paar Dinge klarer formuliert.
Man könnte zum Beispiel aus der, eingebauten, Mastodon Datenschutzerklärung eine potenzielle 12-monatige Speicherung von Webserverlogs ableiten. Natürlich ist das nicht notwendig und wir tun dies auch nicht.<br><br>
Die Versionen sind unter folgenden Links abrufbar:<br>

-) Mastodon: <a href="https://social.anoxinon.de/terms">Alte Version</a> / <a href="/datenschutzerklaerungmastodonneu">Neue Version [Offline seit Februar 2019]</a>
-) XMPP: <a href="/datenschutzerklaerungxmppserver">Alte Version</a> / <a href="/datenschutzerklaerungxmppserverneu">Neue Version [Offline seit Februar 2019]</a>
<br><br>
**Die Nutzer werden noch einmal gesondert zu diesem Blog Beitrag informiert.**
Sollte jemand nicht einverstanden sein, kann er jederzeit sein Konto eigenhändig löschen oder von uns löschen lassen. Ein weiterer Betrieb unter der alten Datenschutzerklärung und dem privaten Betreiber ist aus technischen und organisatorischen Gründen nicht möglich.

<hr>

Soweit war es das für das Dezember Update. Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!
Uns bleibt noch euch frohe Festtage und einen guten Rutsch ins neue Jahr 2019 zu wünschen! Wir sind gespannt was uns erwartet. :)
<br><br>
Viele Grüße,
<br><br>
euer Anoxinon Team
<hr>
