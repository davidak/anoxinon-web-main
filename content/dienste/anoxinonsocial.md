+++
title = "Anoxinon Social"
description = "Mastodon Instanz"
weight = 1
draft = false
bref = "Anoxinons hauseigene Mastodon Instanz im Fediverse"
+++
<p class="left">
Anoxinon Social ist ein Server (Instanz) im sogenannten "Fediverse".<br>
Das Fediverse ist ein dezentrales Kommunikationsnetz, vergleichbar mit der Verwendung von E-Mails.<br>
Im Fediverse gibt es viele verschiedene Anbieter von Servern, die miteinander kommunizieren. Sollte ein <br>Knotenpunkt ausfallen, schränkt dass den Datenverkehr aufgrund der vermaschten Infrastruktur nicht ein.<br><br>
Die Kommunikation  z. B. bei Twitter oder Facebook verläuft über zentrale Server des Anbieters. (Walled Gardens)<br> Fällt dieser Hub einmal aus, kann niemand mehr über diesen Service kommunizieren.<br><br>

Somit liegt der Vorteil dezentraler Netzwerke auf der Hand: Technische Redundanz sorgt für stete Erreichbarkeit der auf den Servern betriebenen Services. Weitere Vorteile sind die Unabhängigkeit von o. g. Firmen, von Datenschutz-Themen mal ganz abgesehen, sowie die Möglichkeit die Instanz zu wechseln, wenn man einem Anbieter nicht mehr vertraut, ohne damit vom Netzwerk ausgeschlossen zu sein.<br><br>

Die Kernthemen unserer Instanz drehen sich derzeit um: Allgemeines, Technik, Datenschutz und Föderation.<br>
Jedoch ist natürlich jeder Willkommen!
<br><br>
<b>Vorteile von Mastodon:</b>
<br>
-) 500+ Zeichen Limit (Twitter bietet 240)<br>
-) Keine Analyse von Daten zu Werbezwecken<br>
-) Große Gemeinschaft, man muss nicht auf demselben Server registriert sein oder dieselbe Software benutzen<br>
-) Dezentrale Struktur<br>
-) Filteroptionen<br>
-) GIF Avatare und Hintergrundbilder<br>
-) 2-Factor Authentication<br>
Und noch viel mehr, einfach Registrieren und sich selber ein Bild davon machen ;)<br><br>

Die von uns eingesetzte Software "Mastodon" ist quelloffen und für jeden frei einsehbar.
<br><br>
<div id="action-buttons">
  <a class="button primary big" href="https://social.anoxinon.de">Zu Mastodon</a> |
    <a class="button primary big" href="https://anoxinon.de/blog/halcyon/">Zu Halcyon</a>
    <p><a href="https://github.com/tootsuite/mastodon">Source Code</a> | <a href="https://notabug.org/halcyon-suite/halcyon/">Source Code</a></p><div>
</p>
