+++
description = "Content Plattform"
title = "Anoxinon Media"
draft = false
weight = 2
bref="Content Plattform des Anoxinon e.V"
toc = true
+++

Anoxinon Media ist eine Plattform des Vereins und erfüllt dessen Zweck die Allgemeinheit über Themen wie Datensicherheit, Verschlüsselung, Datenschutz, Kryptographie, Zensur, sowie den dazugehörigen Begleitthemen zu informieren. In regelmäßigen Abständen, publizieren wir Inhalte die auf verschiedene Zielgruppen zugeschnitten sind.

Derzeit bieten wir Inhalte nur in Textform an, in Zukunft sollen aber noch Videos und Podcasts hinzukommen.<br>
Weitere Informationen kann man der Website entnehmen.


<br><br>
<div id="action-buttons">
  <a class="button primary big" href="https://anoxinon.media">Zu Anoxinon Media</a></div>
