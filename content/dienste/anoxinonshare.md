+++
title = "Anoxinon Share"
description = "Anoxinon Share - File Uploader"
weight = 4
draft = false
bref = "Anoxinons hauseigener Upload Service"
+++

Anoxinon Share ist ein File Upload Dienst der zur Unterstützung unseres XMPP Servers "Anoxinon.me" erstellt wurde.
Mit "Share" besteht die Möglichkeit  Dateien mit einer Größe von bis zu 50 MB versenden zu können.

Der Dienst ist dazu in der Lage jegliche Art von Files für 24h zu speichern, danach werden diese Automatisch gelöscht.
Desweiteren werden alle hochgeladenen Dateien serverseitig verschlüsselt und können zusätzlich per Passwort vor
ungewünschten fremden Download Partnern geschützt werden.  
<br>
<br><br>
<div id="action-buttons">
  <a class="button primary big" href="https://share.anoxinon.de">Zu Anoxinon Share</a></div>
