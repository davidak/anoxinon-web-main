+++
description = "XMPP Server"
title = "Anoxinon Messenger"
draft = false
weight = 3
bref="XMPP Server aus Deutschland"
toc = true
+++
Hier kannst Du Dir einen Account auf dem Anoxinon.me XMPP Server anlegen.<br>
Über deine XMPP ID nutzername@anoxinon.me, können dich später andere Nutzer kontaktieren.<br>

<iframe src="https://xmpp.anoxinon.me/register/new" style="height: 900px; width: 100%;" frameBorder="0"></iframe>

Solltest Du Fragen zu dem Server oder Probleme haben, konsultiere bitte zuerst unsere <a href="https://anoxinon.de/faq">FAQ</a>.<br>
Dort findest Du auch unsere Kontaktmöglichkeiten. Viel Spaß bei der Nutzung unseres XMPP Servers!

**Neu:** Wir bieten jetzt auch einen <a href="https://webchat.anoxinon.me/">Webchat</a> an. Probiert Ihn doch mal aus. :)

<!--<center><a href='https://compliance.conversations.im/server/anoxinon.me'><img src='https://compliance.conversations.im/badge/anoxinon.me'></a>  |  <a href='https://xmpp.net/result.php?domain=anoxinon.me&amp;type=server'><img src='https://xmpp.net/badge.php?domain=anoxinon.me' alt='xmpp.net score' /></a></center>-->
