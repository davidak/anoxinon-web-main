+++
description = "Community Kommunikationsplattform"
title = "Mumble"
draft = false
weight = 6
bref= "Die Kommunikationsplattform für dich und mich"
toc = true
+++
Unser Mumble Server ist öffentlich verfügbar und für jeden nutzbar.<br>
Hier kannst du dich auf voll und ganz mit deinen Freunden austoben, <br>ob bei entspannten
Gesprächen oder bei ein paar Spielen nach der Arbeit.<br><br>
Die Erstellung von Themen bezogenen sowie Privaten
Channeln ist bei uns ohne Probleme möglich, <br>sofern diese aktiv genutzt werden.
Solltest du also noch auf der Suche nach einen verlässlichen stabilen Server sein,<br> auf dem du immer gern
gesehen bist, dann scheue nicht vorbei zu kommen.<br>
<br>
<br>
<!--<h3 class="section-head" id="h-downloads">Verbinden zum Server</a></h3>-->
<div id="action-buttons">
    <a class="button primary big" href="mumble://mumble.anoxinon.de">Verbinden</a>
  <p>Mumble Server Adresse: <b>mumble.anoxinon.de</b> (Immer aktuell)</a></p>
</div>
Natürlich setzen wir auch bei unserem VoIP Dienst auf eine hochwertige Verschlüsselung, so das eine TLS Verschlüsselung nach Perfect Forward Secrecy zum Standard gehört. Die aktive Verschlüsselung könnt ihr am grünen Status Balken des Servers erkennen.
