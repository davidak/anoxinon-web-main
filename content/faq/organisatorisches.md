+++
title = "Organisatorisches"
description = "Anoxinon"
weight = 4
draft = false
bref="Anoxinon"
+++
---
**Wie kann man den Verein erreichen, am besten verschlüsselt? Welchen Ansprechpartner gibt es?**

Den Verein erreicht man am besten via GPG verschlüsselter E-Mail.

Da je nach Anliegen der Ansprechpartner variiert, sammeln wir alle E-Mails zentral an einer Adresse und geben diese an die zuständige Abteilung weiter. Wir erwägen jedoch in Zukunft die verschiedenen Abteilungen einzeln auf der Website aufzulisten.

Weitere Informationen zur Kontaktaufnahme findest Du [hier](https://anoxinon.de/kontakt).

---

**Ist der Ansprechpartner zur Verschwiegenheit verpflichtet?**

Alle ehrenamtlichen Mitarbeiter, ins-besonders technische Administratoren, haben eine vollumfängliche Verschwiegenheitsverpflichtung unterzeichnet. Ihr könnt also sicher sein, dass euer Anliegen und die gespeicherten Daten vertraulich behandelt werden. ;)

---

**Wie lange dauert eine Antwort ungefähr?**

Je nach Tageszeit und der Anzahl verfügbarer Personen, kann eine Antwort bis zu 48 Stunden dauern. Die meisten Anfragen werden jedoch innerhalb weniger Stunden von uns beantwortet, dringende Fälle natürlich priorisiert.

---

**Wie werden User über Veränderungen informiert?**

Die User werden in regelmäßigen Blog Beiträgen über Änderungen informiert. Außerdem  informieren wir vorab auf unserer Status Seite und auf Mastodon über anstehende Wartungsarbeiten, wie Updates oder dergleichen.

---

**Wo stehen die Server bzw. wo genau liegen die Daten?**

Pauschal lässt sich das nicht so einfach sagen. Alle Server befinden sich in Deutschland, der größte Teil davon in Nürnberg & Falkenstein.

---

**Welcher Stromanbieter wird für die Server verwendet?**

Auch diese Frage lässt sich nicht pauschal beantworten, je nach Server Anbieter gibt es einige Unterschiede. Wir versuchen jedoch auf Anbieter zu setzen, die Ihren Strom hauptsächlich aus erneuerbaren Quellen beziehen.

---

**Muss der Server für ein Backup angehalten werden?**

Nein, während des Backups sollte es zu keiner Beeinträchtigung der Dienste kommen.

---

**Wer hat alles Zugriff auf die Daten bzw. werden diese protokolliert?**

Auf unsere Server haben ausschließlich verpflichtete Administratoren Zugriff.
Wir teilen zusätzlich streng nach Zuständigkeitsbereich. Ein Administrator für den XMPP Server kann z.B. nicht auf den Mastodon Server zugreifen. Ausgenommen davon sind Personen, die Bereichsübergreifend arbeiten.

---

**Wie wird der Datenschutz überprüft?**

Derzeit gibt es regelmäßige interne Überprüfungen, aber noch keinen Datenschutzbeauftragten. In Zukunft werden wir aber jemanden ernennen.

---

**Welcher Web-Server wird verwendet?**

Wir setzen auf eine aktuelle Version von Nginx.

---

**Auf welchem Betriebssystem laufen die Server?**

Wir setzen bei all unseren Servern auf Linux, genau genommen Debian.

---

**Wie hoch sind die monatlichen Kosten für den Betrieb der Server bzw. wie lange ist die Finanzierung gesichert?**

Wir veröffentlichen quartalsweise eine Übersicht über die Ausgaben und die Nachhaltigkeit der Finanzierung. Natürlich kann sich aber jeder gerne an der Finanzierung beteiligen. Bitte besucht dafür den Menüpunkt "Unterstützung".

---
