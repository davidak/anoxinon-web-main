+++
title = "Account"
description = "Anoxinon Messenger"
weight = 1
draft = false
bref="Anoxinon Messenger"
+++

<p class="left"><hr>
<b>Wie legt man einen Account an?</b>
<br><br>
Einen Account kann man derzeit nur via Webregistrierung auf anoxinon.me anlegen. Eine Registrierung über App schließen wir derzeit aus.
<br><br><hr>
<b>Wie kann man den Account wieder löschen?</b>
<br><br>
Abhängig vom Client, stellt dieser eine Möglichkeit bereit das Konto auf dem Server zu löschen. Ein Beispiel für PC‘s wäre Gajim, auf Android unter Conversations ist das unseres Wissen nach derzeit nicht möglich.
<br><br><hr>
<b>Werden dabei alle Daten des Accounts sicher gelöscht?</b>
<br><br>
Sobald die Löschanfrage via Client an den Server übertragen wurde, werden alle vorhandenen Informationen auf dem Server gelöscht und bleiben nicht erhalten. Es kann sein, dass gewisse Informationen im regelmäßigen Backup erhalten bleiben. Dieses wird aber fortlaufend ersetzt, die Löschung passiert also automatisch nach Ablauf des Backup Zeitraumes.
<br><br><hr>
<b>Wie wird mit verbotenen Inhalten umgangen bzw. welche Inhalte sind verboten?</b>
<br><br>
Wir möchten in diesem Fall auf unsere <a href="https://anoxinon.de/nutzungsbedingungenxmpp/">Nutzungsbedingungen</a> verweisen. Diese schlüsseln klar und deutlich auf was verboten ist. Dort sind auch etwaige Sanktionen aufgeführt.
<br><br><hr>
<b>Wie ändere ich mein Passwort?</b><br><br>

Viele XMPP Clients bieten die Funktion zum Passwort ändern von Haus
aus an. Bitte vermeide Umlaute wie ä,ü & ö in deinem Passwort, dies
"kann" dazu führen, dass Du Dich nicht mehr einloggen kannst.
<br><br><hr>
<b>Wie kann ich mein Passwort zurücksetzen?</b>
<br><br>
Im Falle eines Passwortverlustes sende uns bitte eine E-mail.
Da wir bei der Registration keine weiteren Daten (bspw. Email Adresse,
Sicherheitsfragen) abfragen, musst Du für ein Passwort Reset die Account Inhaberschaft beweisen.<br>

Dies kannst Du bspw. durch Anlegen eines Fake
Kontakts ala “Totalgeheimer Kontakt” den nur Du kennst tun. Alternativ kannst Du uns auch deine Kontaktliste und weitere Details nennen, womit wir dich als eindeutigen Accountinhaber identifizieren können.
<br><br>
<b><u>Bitte beachte: Wir werden deinen Account nicht wiederherstellen wenn die Inhaberschaft nicht eindeutig nachgewiesen werden kann.</b></u>
<br><br><hr>
<b>Verfügt der XMPP Server über ein Hidden Service für Tor?</b>
<br><br>
Ja der Server kann über folgende Adresse angesprochen werden: 6s3togbna5qzd7tq.onion<br>
<br><hr>
</p>
