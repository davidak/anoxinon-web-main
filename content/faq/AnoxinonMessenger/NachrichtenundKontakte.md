+++
title = "Nachrichten & Kontakte"
description = "Anoxinon Messenger"
weight = 2
draft = false
bref="Anoxinon Messenger"
+++
<p class="left"><hr>
<b>Sind meine Nachrichten verschlüsselt?</b>
<br><br>
Standardmässig werden deine Nachrichten ohne Ende-zu-Ende
Verschüsselung versendet. Du kannst aber via OMEMO deine Nachrichten
verschlüsseln. Nachrichten können dann nur noch von verifizierten
Geräten ausgelesen werden.
<br><br><hr>
<b>Wieso kann Ich einen Kontakt von dem Server XYZ nicht erreichen?</b>
<br><br>
Es kann sein, dass dieser Server eine unsichere (bspw. unverschlüsselte) Verbindung benutzt.
<br>
<b>Anoxinon.me reagiert ausschließlich auf verschlüsselte Verbindungen
von anderen XMPP Servern um die Transportverschlüsselung zu
gewährleisten.</b>
<br>
Veraltete Verschlüsselungsprotokolle wie TLSv1,TLSv1.1, SSLv2
oder SSLv3 werden, ebenso wie selbst signierte Zertifikate, nicht
unterstützt.

<br><br><hr>
<b>Existiert eine Limitierung für den HTTP File Upload?</b>
<br><br>
Ja, diese beträgt 25MB.
<br><br><hr>

</p>
