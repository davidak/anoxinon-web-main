+++
title = "Technisches"
description = "Anoxinon Messenger"
weight = 2
draft = false
bref="Anoxinon Messenger"
+++
<p class="left"><hr>

<b>Welche XMPP Clients könnt Ihr empfehlen?</b>
<br><br>
Für <b>Android “Conversations”</b> (Download via F-Droid, OMEMO ist standardmässig integriert),

für <b>iOS "ChatSecure”</b> (OMEMO ist standardmässig integriert) und

für <b>Windows/Linux "Gajim”</b> (OMEMO muss per Plugin nachgerüstet werden).
<br><br><hr>
<b>Was für Daten werden auf dem Server gespeichert und wie lange? Was für Metadaten fallen an?</b>
<br><br>
In diesem Fall möchten wir auf unsere <a href="https://anoxinon.de/datenschutzerklaerungxmppserver/">Datenschutzerklärung</a> verweisen, die ausführlich beschreibt was und wie gespeichert wird.
<br><br><hr>
<b>Ich bekomme manche Push Benachrichtigungen nicht, woran liegt das?</b>
<br><br>
Grundsätzlich kann es an der Batterieoptimierung Deines Smartphones
liegen oder daran, dass Du die Benachrichtigungen erst gar nicht
aktiviert hast. Solltest Du Hilfe benötigen, kannst Du uns gerne
kontaktieren [<a href="/kontakt/">Kontakt</a>] oder in unserem MUC nachfragen (anoxinon@conference.anoxinon.me).
<br><br><hr>
<b>Wieso funktioniert OMEMO nicht in einem MUC (Multi User Chat)?</b>
<br><br>
OMEMO funktioniert nur in geschlossenen MUCs, wobei jeder Teilnehmer verifiziert werden muss.
<br>
OMEMO in MUCs kann zu Problemen zwischen Clients, bspw. Conversations und Gajim, führen.
<br>
Wenn richtig angewendet, sollte es aber ohne Probleme funktionieren.
<br><br><hr>
<b>Mit welchem FOSS Protokoll arbeitet der Server?</b>
<br><br>
Auf dem XMPP Server setzen wir derzeit Ejabberd mit verschiedenen Erweiterungen (XEP) ein. <br>Eine Übersicht findet Ihr <a href="https://compliance.conversations.im/server/anoxinon.me/">hier</a>.
<br><br><hr>
</p>
