+++
draft= false
title = "Unterstützung"
description = "Mitgliedschaft und Spenden"
+++
---
**Vorwort:**    

Der Anoxinon e.V. möchte, neben der Bereitstellung von öffentlich verfügbaren Diensten, hauptsächlich Aufklärungsarbeit und Information rund um die <a href="/ueberuns/">Vereinsthemen</a> betreiben. Wir finanzieren uns ausschließlich über Mitgliedsbeiträge, Spenden und ggf. Zuschüssen.

Für einen Ausbau unseres Angebots, die Verbesserung unserer Inhalte und zur Sicherung unseres Betriebs, benötigen wir entsprechende Geldmittel. Als Beispiel möchten wir die grafische Aufwertung, den Druck von Werbemitteln, wie Flyern oä, die "offline" Präsenz und den Ausbau unserer Onlinedienste nennen.

---

**Transparenz:**

Unsere derzeitigen monatlichen Betriebskosten belaufen sich auf 73,24€. Rechnet man die jährlichen Betriebskosten, wie Domains uä., dazu betragen die mtl. Betriebskosten etwa 115€.
Das mtl. Beitragsaufkommen beträgt 235€.

Einmalige Kosten, wie Notargebühren, Auslagen und Investitionen, sind hier nicht mit inbegriffen.

Derzeit hat der Verein 16 Mitglieder.

[Stand: 01.01.2020, wird regelmäßig aktualisiert]

---

**Mitgliedschaft**

Wir bieten zwei Arten einer Mitgliedschaft an.

+ **Mitglied:**

Als Mitglied unterstützt man den Verein aktiv, nimmt am Vereinsleben teil und hat, unter anderem, Stimmrechte auf der Mitgliederversammlung, das aktive und passive Wahlrecht, sowie umfassende Auskunftsrechte.

+ **Fördermitglied:**

Ein Fördermitglied unterstützt den Verein primär durch einen finanziellen Beitrag. Als Fördermitglied hat man keine Stimm- bzw. Wahlrechte, dennoch bestehen auch hier Auskunftsrechte.

**Downloads:** <a href="/files/Mitgliedsantrag.pdf">Mitgliedsantrag</a> | <a href="/files/Informationsblatt_Datenschutz.pdf">Informationsblatt_Datenschutz.pdf</a> | <a href="/files/Satzung.pdf">Satzung</a> | <a href="/files/Beitragsordnung.pdf">Beitragsordnung</a>

Eine Mitgliedschaft beim Anoxinon e.V. ist steuerlich absetzbar. Gerne stellen wir eine entsprechende Bescheinigung, auch unter einer Jahressumme von 200€, aus. Anfragen diesbezüglich sind bitte an den <a href="mailto:patrick.vondendriesch@anoxinon.de">Schatzmeister</a> zu richten. Vielen Dank. :)

---

**Spenden:**

Gerne nehmen wir auch einzelne oder wiederkehrende Spenden an.

+ **Überweisung:**

```
IBAN: DE50 5003 1000 1065 1040 04
BIC: TRODDEF1XXX
Kontoinhaber: Anoxinon e.V.
```

+ **Barspende:**

Bei dieser Methode empfiehlt es sich, die Geldscheine bspw. mit einem oder zwei Blatt Papier zu umgeben, damit der Inhalt von außen nicht ersichtlich bzw. ertastbar ist.

```
Anoxinon e.V.
c/o Christopher Bodtke
Jahnstraße 3, 14513 Teltow
Deutschland
```

Spenden an den Anoxinon e.V. sind ebenso steuerlich absetzbar wie eine Mitgliedschaft. Gerne stellen wir eine entsprechende Bescheinigung, auch unter einer Jahressumme von 200€, aus. Voraussetzung dafür ist aber eine **nicht-anonyme** Spende. Anfragen diesbezüglich sind bitte an den <a href="mailto:patrick.vondendriesch@anoxinon.de">Schatzmeister</a> zu richten.

---
