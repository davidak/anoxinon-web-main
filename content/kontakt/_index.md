+++
draft= false
title = "Kontakt"
description = "Bei Fragen kontaktiere uns doch! :)"
+++
<br>
Kontakt via E-Mail: <a href="mailto:postfach@anoxinon.de">postfach[ett]anoxinon(dot)de</a><br>
GPG Key <a href="/pubkeys/anoxinonpubkey.txt">0x6E51ED7057E38007</a> |  ID:  A3B5 2B42 C14E 306F F4E1 BC7B 6E51 ED70 57E3 8007<br>
Kontakt via Mastodon: <a href="https://social.anoxinon.de/@anoxinon">Anoxinon@social.anoxinon.de</a>  <br>
Kontakt via Mumble:  <a href="mumble://mumble.anoxinon.de">mumble.anoxinon.de</a><br>
<br>
Zum Plausch lädt auch unser XMPP MUC (Multi User Chat): <a href="xmpp://anoxinon@conference.anoxinon.me?join">anoxinon@conference.anoxinon.me</a>
<br><br>
