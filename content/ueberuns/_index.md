+++
draft= false
title = "Über uns"
description = "Über uns - Anoxinon"
+++
<br>
Anoxinon ist ein nicht-kommerzieller Verein, <br>dessen Ziel es ist, Bürger zu Informieren und freie Software zu stärken.

[Aktuelle Satzung](https://anoxinon.de/files/Satzung.pdf)

<b>Unsere Hauptanliegen sind:</b>

-) Aufklärungsarbeit & Information in Form von Schrift, VOD, Audio etc., unter anderem über: <br> Dezentrale Kommunikation, Datensicherheit, Verschlüsselung, Datenschutz und Zensur.<br>
-) Austausch mit und Förderung von Open Source Projekten<br>
-) Bereitstellung von dezentralen Kommunikationsplattformen<br>

Weitere Informationen kann man unserem <a href="/blog/gruendungsupdate/">Blog Beitrag zur Gründung </a> entnehmen.


Der Verein ist weltanschaulich sowie politisch unabhängig.

<b>Woher kommt der Name Anoxinon?</b>

"Ano" steht für Anonymous, das "X" für Mister X, dem Unbekannten. Das "I" ist für den Klang des Namens und "non" bezieht sich auf das informatische None, dem Nullwert. 

<hr>
**Vorstand:**
<br><br>
<div class="example">
  <nav class="tabs" data-component="tabs">
    <ul>
      <li class="active">
        <a href="#tab1">Christopher</a>
      </li>
      <li>
        <a href="#tab2">Michael</a>
      </li>
      <li>
        <a href="#tab3">Malte</a>
      </li>
      <li>
        <a href="#tab4">Patrick</a>
        </li>
    </ul>
  </nav>
  <div id="tab1">
    <h5>Christopher B. (SkyfaR), 1. Vorsitzender</h5>
    <p>Chaotischer, kreativer Denker mit Hang zur Perfektion.<br>
     Interessiert sich für Technik (ua. FOSS), Forschung & Musik.<br>
    Zuständig für die allgemeine Organisation. </p>
  </div>
    <div class="hide" id="tab2">
    <h5> Michael G. (angrytux), Vorstandsmitglied </h5>
    <p>Idealist und Geek.<br>
     Interessiert sich für Linux, FOSS, Datenschutz und Gaming.<br>
    Zuständig für Projekte und was sonst eben anfällt. </p>
    </div>
    <div class="hide" id="tab3">
      <h5>Malte K. (beli3ver), Vorstandsmitglied </h5>
      <p>"Wir versuchen, die Weltherrschaft an uns zu reißen."<br>
     Interessiert sich für Linux, OSS, Android und Webentwicklung.<br>
    Zuständig für die technische Infrastruktur und alle Belangen im Bereich der Technik.</p>
  </div>
  <div class="hide" id="tab4">
    <h5> Patrick vDD. (Patrick), Schatzmeister </h5>
folgt
</div>
  </div>

</div>
<hr>
