+++
draft= false
title = "Nutzungsbedingungen"
description = "Anoxinon Messenger"

+++
<br>
Die nachfolgenden Regeln werden mit der Erstellung eines Accounts akzeptiert und gelten für sämtliche Dienste die unter der Domain „anoxinon.me“ angeboten werden. Dieser Dienst ist ein Ort, an dem Menschen unterschiedlicher Herkunft und Art zusammenkommen. Wie überall müssen daher gewisse Regeln gelten, deren Einhaltung durch das Administrations & Moderations Team überwacht wird.
<br>
<hr>
Oberstes Gebot sollte die gegenseitige Rücksichtnahme sein. Die Tatsache dass dies ein öffentlicher Internetdienst ist und man sich nicht "real" gegenübersteht, entbindet nicht von den Grundregeln menschlichen Zusammenlebens und einem Mindestmaß an Benehmen.<br>
<br>
<hr>
<b>Allgemeines</b>
<br> <br>
1) Alle Verstöße oder Aufrufe zum Verstoß gegen deutsches Recht sind untersagt und werden geahndet.
<br><br>
<hr>
<b>Beiträge</b>
<br><br>
2) Beiträge [ua. Nachrichten, Bilder, Dateien etc.] mit rassistischem, illegalen, pornographischem oder gewaltverherrlichendem Inhalt sind streng untersagt. Unter diese Regel fallen auch Avatare und Verweise zu Seiten mit entsprechenden Inhalten. (bspw. Cracks, Hacks & Warez)
<br><br>
2.1) Das massenweise Versenden von Nachrichten oder Datein (sogenanntes Spamming) in einem kurzen Zeitraum ist untersagt.
<br><br>
2.2) Die Veröffentlichung von E-Mails, PNs, Ingame-Messages, TS³/XMPP-Querys oder Gesprächen aus nicht öffentlichen Channeln ist nur dann erlaubt, wenn alle beteiligten Parteien ihre Erlaubnis dazu gegeben haben.
<br><br>
2.3) Beleidigungen und Belästigungen sind generell verboten. Darunter fallen auch jede Form der Diffamierung, Hetze, Übler Nachrede und dergleichen.
<br><br>
<hr>
<b>Account</b>
<br><br>
3) Jedem Nutzer steht maximal ein Account zu. Mehrfachanmeldungen sind nur mit schriftlicher Zustimmung der Administration gestattet. (Bspw. Zur Erstellung eines Projektaccounts)
<br><br>
3.1) Die Administration behält sich vor, dem User die Lizenz zur Nutzung des Dienstes zeitweise oder dauerhaft, jederzeit und ohne Angaben von Gründen zu entziehen. Ausgenommen davon sind gesetzliche Verpflichtungen und Bestimmungen.
<br><br>
<hr>

<b>Bots/Bugs</b>
<br><br>
4) Das Betreiben von Bots ist generell verboten. Ausnahmen benötigen die schriftlichen Zustimmung der Administration.
<br><br>
5) Bugs und Fehler sind der Administration zu melden. Ein bewusstes Ausnutzen dieser Bugs ist untersagt.
<br><br>
<hr>
<b>Sanktionen</b>
<br><br>
6) Zuwiderhandlungen gegen diese Regeln werden normalerweise mit Ermahnung, Verwarnung oder Sperre geahndet. Bei schweren Verstößen, oder Verdacht auf Mutwilligkeit beim Verstoß behalten wir uns die Sperrung aller Accounts bis hin zu einem Hausverbot für das gesamte Anoxinon.de Netzwerk vor. Sperrungen von Accounts sind grundsätzlich personenbezogen. Bei einer Accountsperre darf kein weiterer Account von demselben User erstellt oder benutzt werden.
<br><br>
<hr>
<b>Schlussworte</b>
<br><br>
Wenn es Unklarheiten bezüglich einer Regel gibt, wird die Administration je nach Fall entscheiden. Wir behalten uns vor diese Nutzungsbedingungen jederzeit zu ändern.
