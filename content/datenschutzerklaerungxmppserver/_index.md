+++
draft= false
title = "Datenschutzerklärung (XMPP)"
description = "Anoxinon Messenger"

+++
<br>
**I. Allgemeines zur Datenverarbeitung**<br><br>
Wir verarbeiten personenbezogene Daten unserer Nutzer grundsätzlich nur, soweit dies
zur Bereitstellung einer funktionsfähigen Website sowie unserer Inhalte und Leistungen
erforderlich ist. Wir benutzen keine Analyse Software und binden auch keine Drittanbieter (Google Fonts etc.) ein. Zur leichteren Lesbarkeit sind wichtige Informationen im Text fett hervorgehoben.<br><br>
Die **Registration** eines XMPP Kontos auf "Anoxinon.me" ist Grundsätzlich kostenlos<br>
und **kann ohne Angabe von personenbezogenen Daten erfolgen**. <br>
Aus diesem Grund könnt ihr euch bei einem Passwort Verlustes auch nur schwer legitimieren. <br>Wie ihr dennoch ein neues
Kennwort erhalten könnt findet ihr in unseren "<a href="/faq">FAQs</a>".<br><br>
<hr>
**II. Verantwortliche Stelle**<br><br>
Der Verantwortliche im Sinne der Datenschutz-Grundverordnung ist:<br><br>
**Anoxinon e.V.**<br>
c/o Christopher Bodtke<br>
Jahnstraße 3<br>
14513 Teltow<br>
Telefon: (+49) 157 / 3223 9490<br>
Mail: postfach@anoxinon.de<br>
Amtsgericht Potsdam; Registernummer: 9009<br><br>
<hr>

**III. Einsatzgebiet dieser Datenschutzerklärung** <br><br>
Diese Datenschutzerklärung ist für den XMPP Dienst „anoxinon.me“ gültig.
<br>Sie gilt ergänzend zu der <a href="/datenschutzerklaerung">Datenschutzerklärung</a> von Anoxinon.de.<br><br>
<hr>
**IV. Erhebung und Verarbeitung personenbezogener Daten**<br><br>

**1. Bereitstellung des XMPP Dienstes**

Bei der Registrierung und Nutzung des XMPP-Dienstes werden folgende Daten erfasst:<br>
```
  Benutzername
  Passwort
  IP-Adresse des Clients
  PEP-Accountinformationen, falls eingerichtet
  XMPP-Kontakte (XMPP-IDs) und zugeordnete Namen
  Versendete Nachrichten
  Onlinestatus (Status, Zeitpunkt der letzten Änderung)
  Dateiuploads
  Technische Informationen über den verwendeten Client (unterstützte (Protokoll-/) Versionen)
  Zeitpunkt des letzten Logins / Logouts
```

Die **IP-Adresse wird nur bei fehlerhaften Loginversuchen** in einer Logdatei hinterlegt und wird im normalen Betrieb nicht protokolliert. Die längerfristige Protokollierung von Nachrichten ist **standardmäßig deaktiviert**. Über eine Clientfunktion kann die Nachrichtenprotokollierung (MAM) dauerhaft eingeschaltet werden, sodass die Nachrichten nicht nur temporär zu Übermittlungszwecken gespeichert werden.<br><br>
**Logdateien** inkl. gespeicherter IP-Adressen werden **nach spätestens 72 Stunden, Nachrichtenprotokollen (MAM) und Dateiuploads nach 2 Wochen gelöscht.**<br><br>
Die oben aufgelisteten Informationen dienen der Bereitstellung des Dienstes und müssen gespeichert werden, um dem Benutzer den Service anbieten zu können. Die Speicherung von PEP-Accountinformationen und Dateiuploads geschieht nur, wenn der Benutzer explizit Funktionen des Dienstes nutzt, die diese zwingend erfordern. (Ausfüllen der Profilinformationen, Austausch von Dateien im Chat).<br><br>
Benutzername, PEP-Accountinformationen, Status, Dateiuploads und Nachrichten werden mit anderen XMPP-Servern geteilt, sofern mit einem Benutzer eines anderen Servers kommuniziert wird. Dies ist bedingt durch die Funktionsweise des dezentralen XMPP-Netzwerks.<br><br>
Der Benutzer kann die erfassten Daten zum persönlichen XMPP-Account **jederzeit auf Anfrage (Clientseitig) löschen lassen**. Eine Weiternutzung des Angebots ist dann ggf. aus technischen Gründen nicht mehr möglich. Ein Widerspruch bzgl. der Erfassung und Verwendung im Rahmen des Angebots ist in der Regel nicht möglich, da der angebotene Dienst sonst nicht erbracht werden kann.<br><br>

Rechtsgrundlage für die Speicherung der Daten ist Art. 6 Abs. 1 lit. b DSGVO.<br><br>

<hr>
**V. Auftragsverarbeitung gemäß Art. 28 DSGVO / Weitergabe von Daten**<br><br>
Der Nutzer erklärt sich damit einverstanden, dass durch Beauftragung der folgenden
Unternehmen zur Erbringung von Dienstleistungen Daten an diese weitergegeben werden:<br><br>
    • Hetzner.de: Serverhosting. Sämtliche Daten, die durch Anbieten von Servern und der anoxinon.de Website anfallen, werden auf Serversystemen der Hetzner Online GMBH gespeichert. Verantwortlicher des Unternehmens ist Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen, Deutschland.<br><br>
Auftragsverarbeitungsverträge mit den genannten Unternehmen liegen vor.<br><br>
Es werden keine Daten an Dritte weitergeben, außer sie haben dazu Ihre Einwilligung nach Art. 6 Abs. 1 a DSGVO erteilt, eine gesetzliche Verpflichtung nach Art. 6 Abs. 1 c DSGVO besteht, oder wenn wir dies für die Vertragsabwicklung ( Art. 6 Abs. 1 b DSGVO) sowie die Geltendmachung, Ausübung und Verteidigung von Rechtsansprüchen benötigen.  (Art. 6 Abs. 1 f DSGVO)<br><br>
<hr>

**VI. Datenlöschung und Speicherdauer**

Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch den Gesetzgeber vorgesehen wurde. Eine Sperrung oder Löschung der Daten erfolgt im Anschluss, wenn die vorgeschriebene Speicherfrist abläuft. Es ist möglich dass trotzdem eine Erforderlichkeit zur weiteren Speicherung der Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.<br><br>
Der Betreiber des Dienstes behält sich vor, sämtliche vom Server gespeicherten Daten in Form von Backups für bis zu einem halben Jahr auf externen Systemen vorzuhalten. (Hinweis: Derzeit nutzen wir diesen Rahmen nicht aus und werden nach Evaluierung den exakten Zeitraum an dieser Stelle bekanntgeben)<br><br>
<hr>
**VII. Rechte der betroffenen Person**
<br><br>
***1. Auskunftsrecht***<br><br>
Sie können von dem Verantwortlichen eine Bestätigung darüber verlangen, ob personenbezogene Daten, die Sie betreffen, von uns verarbeitet werden.
Dazu zählen unter anderem: Kategorie der personenbezogenen Daten, Empfänger, Verarbeitungszwecke, Speicherdauer, Herkunft (sofern die Daten aus Drittquellen kommen), Informationen über automatisierte Entscheidungsfindung, deren Details und ggf. Profiling. (Wir führen die beiden letzten Punkte nicht durch)
Ausserdem können sie Auskunft über Ihre Rechte (Beschwerderecht, Löschung, Berichtigung, Einschränkung der Verarbeitung )verlangen.<br>

***2. Recht auf Berichtigung***<br><br>
Sie haben ein Recht auf Berichtigung und/oder Vervollständigung gegenüber dem
Verantwortlichen, sofern die verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig oder unvollständig sind. Der Verantwortliche hat die Berichtigung unverzüglich vorzunehmen.<br>

***3. Recht auf Einschränkung der Verarbeitung und Löschung***<br><br>
Sie können die Einschränkung der Verarbeitung der Sie
betreffenden personenbezogenen Daten verlangen, wenn Sie die Richtigkeit bestreiten, die Verarbeitung unrechtmäßig ist und Sie die Löschung ablehnen und stattdessen die Einschränkung der Nutzung verlangen, wir diese Daten nicht mehr benötigen, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DSGVO eingelegt haben.<br><br>
Sie können weiters von dem Verantwortlichen verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, und der Verantwortliche ist verpflichtet, dies zu tun, außer die Verarbeitung ist erforderlich zur Ausübung des Rechts auf freie Meinungsäußerung und Information; zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses, zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

***4. Recht auf Datenübertragbarkeit***<br><br>
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem
Verantwortlichen bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Außerdem haben Sie das Recht diese Daten einem anderen Verantwortlichen übermitteln zu lassen.

***5. Widerspruchs und Widerrufsrecht***<br><br>
Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.<br><br>
Sie haben ebenso das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen.<br><br>
Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu
betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung zum Zwecke derartiger Werbung einzulegen.

***6. Recht auf Beschwerde bei einer Aufsichtsbehörde***<br><br>
Ihnen steht das Recht auf Beschwerde bei einer Aufsichtsbehörde zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt. Sie können sich grundsätzlich an die zuständige Aufsichtsbehörde Ihres Wohnortes oder Arbeitsplatz wenden.<br><br>
<hr>

**VIII. Schlussworte**<br><br>

Die letzten Änderungen erfolgten am 28.06.2019. <br>Wir behalten uns vor diese Datenschutzerklärung jederzeit zu ändern.<br> Die aktuelle Version ist jederzeit unter diesem Link zu finden.


<br>
<hr>
<br>
<b>
